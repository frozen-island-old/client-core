/**
 * The Core Game Engine.
 */
export class FrozenIslandEngine {
    /**
     * The constructor for the game engine.
     * @param gameElement The div element in which the game world is to be displayed.
     * @param loadingElement The div element in which the loading screen is stored for displaying.
     * @param errorElement The div element where we put messages. (Mostly errors.)
     */
    constructor(gameElement, loadingElement, errorElement) {
        this.gameElement = gameElement;
        this.loadingElement = loadingElement;
        this.errorElement = errorElement;
    }
}
