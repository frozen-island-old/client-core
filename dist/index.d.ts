/**
 * The Core Game Engine.
 */
export declare class FrozenIslandEngine {
    private gameElement;
    private loadingElement;
    private errorElement;
    /**
     * The constructor for the game engine.
     * @param gameElement The div element in which the game world is to be displayed.
     * @param loadingElement The div element in which the loading screen is stored for displaying.
     * @param errorElement The div element where we put messages. (Mostly errors.)
     */
    constructor(gameElement: HTMLDivElement, loadingElement: HTMLDivElement, errorElement: HTMLDivElement);
}
