
/**
 * The Core Game Engine.
 */
export class FrozenIslandEngine {

    private gameElement: HTMLDivElement;

    private loadingElement: HTMLDivElement;

    private errorElement: HTMLDivElement;
    /**
     * The constructor for the game engine.
     * @param gameElement The div element in which the game world is to be displayed.
     * @param loadingElement The div element in which the loading screen is stored for displaying.
     * @param errorElement The div element where we put messages. (Mostly errors.)
     */
    constructor(gameElement: HTMLDivElement, loadingElement: HTMLDivElement, errorElement: HTMLDivElement){
        this.gameElement = gameElement;
        this.loadingElement = loadingElement;
        this.errorElement = errorElement;
    }
}
